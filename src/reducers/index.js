import { combineReducers } from "redux";
import courses from "./courses";
import filters from "./filters";

export default combineReducers({
  courses,
  filters
});
