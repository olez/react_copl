import { CoursesOSSU as courses } from "../data.js";

const initialState = {
  courses
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case "TOGGLE_COMPELETED":
      return { ...state, ...payload };

    default:
      return state;
  }
};
