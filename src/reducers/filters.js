const initialState = {
  query: null
};

export default (filters = initialState, { type, payload }) => {
  console.log(type, payload);
  switch (type) {
    case "FILTER_ARTICLE":
      return { ...filters, query: payload.query };
    case "DELETE_FILTERED_ITEM":
      return {
        ...filters,
        filtered: filters.filtered.filter(
          item => item.title.search(payload.query) !== -1
        )
      };

    default:
      return filters;
  }
};
