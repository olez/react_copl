export const CoursesOSSU = [
  {
    title: "How to Code - Simple Data",
    duration: "7 weeks",
    effort: "8-10 hours/week",
    prereq: "none",
    id: 0
  },
  {
    title: "How to Code - Complex Data",
    duration: "6 weeks",
    effort: "8-10 hours/week",
    prereq: "How to Code: Simple Data",
    id: 1
  },
  {
    title: "Software Construction - Data Abstraction",
    duration: "6 weeks",
    effort: "8-10 hours/week",
    prereq: "How to Code - Complex Data",
    id: 2
  },
  {
    title: "Software Construction - Object-Oriented Design",
    duration: "6 weeks",
    effort: "8-10 hours/week",
    prereq: "Software Construction - Data Abstraction",
    id: 3
  },
  {
    title: "Programming Languages, Part A",
    duration: "4 weeks",
    effort: "8-16 hours/week",
    prereq: "recommended: Java, C",
    id: 4
  },
  {
    title: "Programming Languages, Part B",
    duration: "3 weeks",
    effort: "8-16 hours/week",
    prereq: "Programming Languages, Part A",
    id: 5
  },
  {
    title: "Programming Languages, Part C",
    duration: "3 weeks",
    effort: "8-16 hours/week",
    prereq: "Programming Languages, Part B",
    id: 6
  },
  {
    title: "Programming Languages, Part C",
    duration: "3 weeks",
    effort: "8-16 hours/week",
    prereq: "Programming Languages, Part B",
    id: 7
  },
  {
    title: "Essence of Linear Algebra",
    duration: "-",
    effort: "-",
    prereq: "pre-calculus",
    id: 8
  },
  {
    title: "Linear Algebra - Foundations to Frontiers (alt)",
    duration: "15 weeks",
    effort: "8 hours/week",
    prereq: "Essence of Linear Algebra",
    id: 9
  },
  {
    title: "Calculus 1A: Differentiation",
    duration: "13 weeks",
    effort: "6-10 hours/week",
    prereq: "pre-calculus",
    id: 10
  },
  {
    title: "Calculus 1B: Integration",
    duration: "13 weeks",
    effort: "5-10 hours/week",
    prereq: "Calculus 1A",
    id: 11
  },
  {
    title: "Calculus 1C: Coordinate Systems & Infinite Series",
    duration: "13 weeks",
    effort: "5-10 hours/week",
    prereq: "Calculus 1B",
    id: 12
  },
  {
    title: "Mathematics for Computer Science1",
    duration: "13 weeks",
    effort: "5 hours/week",
    prereq: "Calculus 1C",
    id: 13
  },
  {
    title: "Introduction to Computer Science - CS50 (alt)",
    duration: "12 weeks",
    effort: "10-20 hours/week",
    prereq: "introductory programming",
    id: 14
  },
  {
    title:
      "Build a Modern Computer from First Principles: From Nand to Tetris (alt)",
    duration: "6 weeks",
    effort: "7-13 hours/week",
    prereq: "C-like programming language",
    id: 15
  },
  {
    title:
      "Build a Modern Computer from First Principles: Nand to Tetris Part II",
    duration: "6 weeks",
    effort: "12-18 hours/week",
    prereq: "one of these programming languages, From Nand to Tetris Part I",
    id: 16
  },
  {
    title: "Introduction to Computer Networking",
    duration: "8 weeks",
    effort: "4–12 hours/week",
    prereq: "algebra, probability, basic CS",
    id: 17
  },
  {
    title: "ops-class.org - Hack the Kernel",
    duration: "15 weeks",
    effort: "6 hours/week",
    prereq: "algorithms",
    id: 18
  },
  {
    title: "Algorithms: Design and Analysis, Part I",
    duration: "8 weeks",
    effort: "4-8 hours/week",
    prereq: "any programming language, Mathematics for Computer Science",
    id: 19
  },
  {
    title: "Algorithms: Design and Analysis, Part II",
    duration: "8 weeks",
    effort: "4-8 hours/week",
    prereq: "Part I",
    id: 20
  },
  {
    title: "Databases",
    duration: "12 weeks",
    effort: "8-12 hours/week",
    prereq: "some programming, basic CS",
    id: 21
  },
  {
    title: "Machine Learning",
    duration: "11 weeks",
    effort: "4-6 hours/week",
    prereq: "linear algebra",
    id: 22
  },
  {
    title: "Computer Graphics",
    duration: "6 weeks",
    effort: "12 hours/week",
    prereq: "C++ or Java, linear algebra",
    id: 23
  },
  {
    title: "Cryptography I",
    duration: "6 weeks",
    effort: "5-7 hours/week",
    prereq: "linear algebra, probability",
    id: 24
  },
  {
    title: "Software Engineering: Introduction",
    duration: "6 weeks",
    effort: "8-10 hours/week",
    prereq: "Software Construction - Object-Oriented Design",
    id: 25
  },
  {
    title: "Software Development Capstone Project",
    duration: "6-7 weeks",
    effort: "8-10 hours/week",
    prereq: "Software Engineering: Introduction",
    id: 26
  },
  {
    title: "Compilers",
    duration: "9 weeks",
    effort: "6-8 hours/week",
    prereq: "none",
    id: 27
  },
  {
    title: "Software Debugging",
    duration: "8 weeks",
    effort: "6 hours/week",
    prereq: "Python, object-oriented programming",
    id: 28
  },
  {
    title: "Software Testing",
    duration: "4 weeks",
    effort: "6 hours/week",
    prereq: "Python, programming experience",
    id: 29
  },
  {
    title: "LAFF - On Programming for Correctness",
    duration: "7 weeks",
    effort: "6 hours/week",
    prereq: "linear algebra",
    id: 30
  },
  {
    title: "Introduction to Parallel Programming (alt)",
    duration: "12 weeks",
    effort: "-",
    prereq: "C, algorithms",
    id: 31
  },
  {
    title: "Software Architecture & Design",
    duration: "8 weeks",
    effort: "6 hours/week",
    prereq: "software engineering in Java",
    id: 32
  },
  {
    title: "Multivariable Calculus",
    duration: "13 weeks",
    effort: "12 hours/week",
    prereq: "MIT Calculus 1C",
    id: 33
  },
  {
    title: "Introduction to Probability - The Science of Uncertainty",
    duration: "18 weeks",
    effort: "12 hours/week",
    prereq: "Multivariable Calculus",
    id: 34
  },
  {
    title: "Reliable Distributed Systems, Part 1",
    duration: "5 weeks",
    effort: "5 hours/week",
    prereq: "Scala, intermediate CS",
    id: 35
  },
  {
    title: "Reliable Distributed Systems, Part 2",
    duration: "5 weeks",
    effort: "5 hours/week",
    prereq: "Part 1",
    id: 36
  },
  {
    title: "Electricity and Magnetism, Part 11",
    duration: "7 weeks",
    effort: "8-10 hours/week",
    prereq: "calculus, basic mechanics",
    id: 37
  },
  {
    title: "Electricity and Magnetism, Part 2",
    duration: "7 weeks",
    effort: "8-10 hours/week",
    prereq: "Electricity and Magnetism, Part 1",
    id: 38
  },
  {
    title: "Computation Structures 1: Digital Circuits",
    duration: "10 weeks",
    effort: "6 hours/week",
    prereq: "electricity, magnetism",
    id: 39
  },
  {
    title: "Computation Structures 2: Computer Architecture",
    duration: "10 weeks",
    effort: "6 hours/week",
    prereq: "Computation Structures 1",
    id: 40
  },
  {
    title: "Computation Structures 3: Computer Organization",
    duration: "10 weeks",
    effort: "6 hours/week",
    prereq: "Computation Structures 2",
    id: 41
  },
  {
    title: "Introduction to Logic",
    duration: "10 weeks",
    effort: "4-8 hours/week",
    prereq: "set theory",
    id: 42
  },
  {
    title: "Automata Theory",
    duration: "7 weeks",
    effort: "10 hours/week",
    prereq: "discrete mathematics, logic, algorithms",
    id: 43
  },
  {
    title: "Computational Geometry",
    duration: "16 weeks",
    effort: "8 hours/week",
    prereq: "algorithms, C++",
    id: 44
  },
  {
    title: "Introduction to Formal Concept Analysis",
    duration: "6 weeks",
    effort: "4-6 hours/week",
    prereq: "logic, probability",
    id: 45
  },
  {
    title: "Game Theory",
    duration: "8 weeks",
    effort: "x hours/week",
    prereq: "mathematical thinking, probability, calculus",
    id: 46
  },
  {
    title: "Robotics (Specialization)",
    duration: "26 weeks",
    effort: "2-5 hours/week",
    prereq: "linear algebra, calculus, programming, probability",
    id: 47
  },
  {
    title: "Data Mining (Specialization)",
    duration: "30 weeks",
    effort: "2-5 hours/week",
    prereq: "machine learning",
    id: 48
  },
  {
    title: "Big Data (Specialization)",
    duration: "30 weeks",
    effort: "3-5 hours/week",
    prereq: "none",
    id: 49
  },
  {
    title: "Internet of Things (Specialization)",
    duration: "30 weeks",
    effort: "1-5 hours/week",
    prereq: "strong programming",
    id: 50
  },
  {
    title: "Cloud Computing (Specialization)",
    duration: "30 weeks",
    effort: "2-6 hours/week",
    prereq: "C++ programming",
    id: 51
  },
  {
    title: "Full Stack Web Development (Specialization)",
    duration: "27 weeks",
    effort: "2-6 hours/week",
    prereq: "programming, databases",
    id: 52
  },
  {
    title: "Data Science (Specialization)",
    duration: "43 weeks",
    effort: "1-6 hours/week",
    prereq: "none",
    id: 53
  },
  {
    title: "Functional Programming in Scala (Specialization)",
    duration: "29 weeks",
    effort: "4-5 hours/week",
    prereq: "One year programming experience",
    id: 54
  }
];
