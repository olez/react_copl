import React from 'react'
import { Navbar, Nav } from 'react-bootstrap';
import SearchFilter from '../filters/index';

export default function Header() {
  return (
    <header className="App-header">
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="/">Course Planner</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/">Home</Nav.Link>
          <Nav.Link href="/courses">All courses</Nav.Link>
          <Nav.Link href="/courses/new">Add new course</Nav.Link>
          <Nav.Link href="">Settings</Nav.Link>
        </Nav>
        <SearchFilter/>
      </Navbar.Collapse>
    </Navbar>
    </header>
  )
}
