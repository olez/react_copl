import React from 'react'
import { Form, FormControl, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import filterArticle from '../../ac/filter'

function SearchFilter(props) {
  const searchInput = React.createRef();
  return (
    <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" ref={searchInput} onChange={handleClick}/>
          {/* <Button variant="outline-success" onClick={handleClick}>Search</Button> */}
    </Form>
  )

  function handleClick(){
      props.filterArticle(searchInput.current.value.toLowerCase());
  }
}

export default connect(null, {filterArticle})(SearchFilter);