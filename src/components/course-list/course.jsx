import React from 'react'
import PropTypes from 'prop-types'

Course.propTypes = {
  course: PropTypes.shape({
    title: PropTypes.string.isRequired,
    duration: PropTypes.string.isRequired,
    effort: PropTypes.string,
    prereq: PropTypes.string,
    done: PropTypes.bool,
  })
}

export default function Course({course}) {
  const { title, 
          duration, 
          effort,
          prereq,
          done } = course;
  console.log('### rendering course.jsx');

  return (
    <li>
      <ul className="list-group list-group-horizontal-lg">
            <li className="list-group-item">{title}</li>
            <li className="list-group-item">{duration}</li>
            <li className="list-group-item">{effort}</li>
            <li className="list-group-item">{prereq}</li>
            <li className="list-group-item courses--course__button"><input type="checkbox" defaultChecked={done}/></li>
            <li className="list-group-item courses--course__button"><button>X</button></li>
        </ul>
    </li>
  )
}
