import React from 'react'
import PropTypes from 'prop-types'
import Course from './course'
import { connect } from 'react-redux'
import { filteredSelector } from '../../selectors';
import UserData from '../user-data'

CourseList.propTypes = {
    courses: PropTypes.array.isRequired
}

function CourseList({courses}) {
  console.log('### rendering course-list');

    return (
      <div>
      <UserData/>
      <ul>
        {courses.map( course => <Course key={course.id} course = {course}/>)}
      </ul>
      </div>
    )
  }

export default connect(state => {
  return { courses: filteredSelector(state)}
}, null)(CourseList)

