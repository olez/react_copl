import React from 'react'
import ProgressBar from 'react-bootstrap/ProgressBar'

function UserData(props) {
  return (
    <div>
    <div>
        <h5>Hello, Username.</h5>
        <h6>Your available time per week is: 12 hours.</h6>
    </div>
    <p>Progress statistics of courses you are assigned to:</p>
    <ProgressBar>
        <ProgressBar striped variant="success" now={15} key={1} />
        <ProgressBar variant="warning" now={65} key={2} />
        <ProgressBar striped variant="danger" now={30} key={3} />
    </ProgressBar>
    </div>
  )
}

export default UserData

