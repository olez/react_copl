import React, { Component } from "react";
import Header from "./components/header";
import CourseList from "./components/course-list";
import User from "./components/user-data";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <div className="container-fluid">
          <Router>
            <div>
              <Route exact path="/user" component={User} />
              <Route path="/courses" component={CourseList} />
              <Route exact path="/" component={CourseList} />
            </div>
          </Router>
        </div>
      </div>
    );
  }
}

export default App;
