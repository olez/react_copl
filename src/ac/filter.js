export default function filterArticle(query) {
  return {
    type: "FILTER_ARTICLE",
    payload: { query }
  };
}
