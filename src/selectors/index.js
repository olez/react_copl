import { createSelector } from "reselect";

const coursesAllSelector = state => state.courses.courses;
const coursesFilteredSelector = state => state.filters;

export const coursesSelector = createSelector(
  coursesAllSelector,
  courses => courses
);

export const filteredSelector = createSelector(
  coursesAllSelector,
  coursesFilteredSelector,
  (courses, filters) => {
    console.log("###", "filtered courses list selector");
    return courses.filter(course => {
      return (
        !filters.query ||
        course.title.toLowerCase().search(filters.query) !== -1
      );
    });
  }
);
